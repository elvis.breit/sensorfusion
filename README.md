# Sensorfusion

Skripte der Bachelorarbeit "Untersuchungen zur Positionsbestimmung mobiler Robotersysteme zur Lasermaterialbearbeitung"

## In diesem Git enthaltene Dateien

In den Ordnern befinden sich die entsprechenden Skripte, auf die sich die Bachelorarbeit bezieht. Die .zip-Datei "all" enthält alle Dateien gepackt.
