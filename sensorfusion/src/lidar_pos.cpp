#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include <vector>
#include <unistd.h>
#include "sensorfusion/Position.h"
#include <cmath>

#define RAD2DEG(x) ((x)*180./M_PI)

using namespace std;

static vector<double> ypos_dist_vec, xpos_dist_vec, min_dist_degree = {};
double x_pos = 0.0, y_pos = 0.0, init_min_y_dist = 0.0, init_min_x_dist = 0.0, min_x_dist = 0.0, min_y_dist = 0.0, init_degree = 0.0, current_degree = 0.0;
static bool init_not_done = true;

class Lidar
{
public:
  Lidar()
  {
    pub_ = nh.advertise<sensorfusion::Position>("/lidar_pos_cov", 1);

    sub_ = nh.subscribe("/scan", 1000, &Lidar::scanCallback, this);
  }


void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{    
    //Amount of measurements = "time between scans" divided by "time between measurements"
    static int count = scan->scan_time / scan->time_increment; 

    /*Determine Initial Distance to walls with Lidar Scanner
    ------------------------------------------*/

    //Do it only, if there is not a initial measurement determined. Check whether distance vectors of x and y are empty    
    if (init_not_done){
        
        for(int i = 0; i < count; i++) {
            
            double degree = RAD2DEG(scan->angle_min + scan->angle_increment * i);

           // Save distance measurements of Lidar (front wall)
	        if(degree > -10.0 && degree < 10.0){
                
                if(scan->ranges[i] < scan->range_min) continue; //filter values with distance less than range_min (default = 0.1 [m])
                ypos_dist_vec.push_back(scan->ranges[i]);
                min_dist_degree.push_back(degree);

            } 
        
            // Save distance measurements of Lidar (left wall)
            if(degree > 80.0 && degree < 100.0) {
                
                if(scan->ranges[i] < scan->range_min) continue; //filter values with distance less than range_min (default = 0.1 [m])
                xpos_dist_vec.push_back(scan->ranges[i]);

            }

            //Save distance measurements of Lidar (right wall)
            //if(degree > -92.5 && degree < -87.5) {
                
              // if(scan->ranges[i] < scan->range_min) continue; //filter values with distance less than range_min (default = 0.1 [m])
              //  xpos_dist_vec.push_back(scan->ranges[i]); 
              //  min_dist_degree.push_back(degree);
              //   
            //}
        
        }

    //Set initial measurements
    init_min_x_dist = *min_element(xpos_dist_vec.begin(), xpos_dist_vec.end()) * 1000.0; //convert m in mm
    
    init_min_y_dist = *min_element(ypos_dist_vec.begin(), ypos_dist_vec.end()) * 1000.0; //convert m in mm

    init_degree = min_dist_degree[min_element(ypos_dist_vec.begin(), ypos_dist_vec.end()) - ypos_dist_vec.begin()]; //Get index of the smallest value
    
    //Clear Vectors with values
    xpos_dist_vec.clear(); ypos_dist_vec.clear();

    //After initialization no further necessary
    init_not_done = false;
    }

    
    /*Determine X and Y Position
    ------------------------------------------*/
 
    for(int i = 0; i < count; i++) {
        
        double degree = RAD2DEG(scan->angle_min + scan->angle_increment * i);

        //Save distance measurements of Lidar (front wall)
	     if(degree > -10.0 && degree < 10.0){
                
                if(scan->ranges[i] < scan->range_min) continue; //filter values with distance less than range_min (default = 0.1 [m])
                ypos_dist_vec.push_back(scan->ranges[i]);
                min_dist_degree.push_back(degree);


            } 
        
        //Save distance measurements of Lidar (left wall)
        if(degree > 80.0 && degree < 100.0) {
                
                if(scan->ranges[i] < scan->range_min) continue; //filter values with distance less than range_min (default = 0.1 [m])
                xpos_dist_vec.push_back(scan->ranges[i]); 

            }

      //   //Save distance measurements of Lidar (right wall)
      //  if(degree > -92.5 && degree < -87.5) {
                
      //           if(scan->ranges[i] < scan->range_min) continue; //filter values with distance less than range_min (default = 0.1 [m])
      //           xpos_dist_vec.push_back(scan->ranges[i]);
      //           min_dist_degree.push_back(degree);

      //       }

    }
        
        sensorfusion::Position msg;
        
        //Determine x and y Position with Difference of current min distance measurement and init measurements
        
        //left and front wall
        min_x_dist = *min_element(xpos_dist_vec.begin(), xpos_dist_vec.end()) * 1000.0; //convert m in mm
        min_y_dist = *min_element(ypos_dist_vec.begin(), ypos_dist_vec.end()) * 1000.0; //convert m in mm
        current_degree = min_dist_degree[min_element(ypos_dist_vec.begin(), ypos_dist_vec.end()) - ypos_dist_vec.begin()];


        setprecision(3);
        //left and front wall
        msg.x = (min_x_dist - init_min_x_dist);
        msg.y = -(min_y_dist - init_min_y_dist);
        msg.azimuth = init_degree - current_degree + 90.0;

        // //right and front wall
        //msg.x = -(min_x_dist - init_min_x_dist);
        //msg.y = -(min_y_dist - init_min_y_dist);
        
        if(min_x_dist <= 1000){msg.x_var = 44.44;} 
        else{msg.x_var = pow(((min_x_dist*0.02)/3.0),2);} //relative error of 2%

        if(min_y_dist <= 1000){msg.y_var = 44.44;} 
        else{msg.y_var = pow(((min_y_dist*0.02)/3.0),2);} //relative error of 2%

        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = "/lidar_frame";
              
        pub_.publish(msg);

        //Clear Vectors with values
        xpos_dist_vec.clear(); ypos_dist_vec.clear();
} 

private:
  ros::NodeHandle nh; 
  ros::Publisher pub_;
  ros::Subscriber sub_;
};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lidar_pos");
  Lidar lidar_obj;
  ros::spin();

  return 0;
}



