#!/usr/bin/env python3
import numpy as np
import math
import csv

x_pos = []
y_pos = []
path_list = []
trajectory = [[1,0],[2,0],[2,2]]

for i in range(len(trajectory)):
    x_pos.append(trajectory[i][0])
    y_pos.append(trajectory[i][1])

print(x_pos)
print(y_pos)    

segment_lengths = (np.diff(x_pos)**2 + np.diff(y_pos)**2)**.5
print(segment_lengths)

cumsum_length = np.zeros_like(x_pos)
print(cumsum_length)

if(len(segment_lengths) == 1):
    np.delete(cumsum_length,0)
    cumsum_length[1] = segment_lengths[0]

elif(len(cumsum_length) < 3):
    cumsum_length[0] = segment_lengths[0]
    cumsum_length[1] = segment_lengths[0] + segment_lengths[1]
    print(cumsum_length)

else:    
    cumsum_length[1:] = np.cumsum(segment_lengths)
    print(cumsum_length)

cumsum_length_int = np.linspace(0,cumsum_length.max(),10)
print(cumsum_length_int)


x_pos_int = np.interp(cumsum_length_int, cumsum_length, x_pos)
y_pos_int = np.interp(cumsum_length_int, cumsum_length, y_pos)

# for i in range(len(x_pos_int)):
#     path_list.append([round(x_pos_int[i],4),round(y_pos_int[i],4)])

# with open('/home/pi/catkin_ws/src/sensorfusion/csv/path.csv', 'w', encoding='UTF8', newline='') as f:
#     writer = csv.writer(f)
#     writer.writerows(path_list)