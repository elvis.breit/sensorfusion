#!/usr/bin/env python3
from typing import Type
from std_msgs.msg import Float64MultiArray, Float64
import serial
import rospy
import numpy as np
from sensorfusion.srv import Trajectory, TrajectoryResponse

#Open Serial to create connection to motor drivers
ser = serial.Serial(
		'/dev/ttyS0', 
		baudrate = 115200,
		parity = serial.PARITY_NONE,
		stopbits=serial.STOPBITS_ONE,
		bytesize =serial.EIGHTBITS,
		timeout = 1
		)

ser.close()
ser.open()

#Save X,Y Position in trajectory
with open('/home/pi/catkin_ws/src/sensorfusion/csv_path/coordinate_system.csv', newline='') as csvfile:
        trajectory = np.loadtxt(csvfile, delimiter=',')

#TODO Check whether angles are correct
def calc_robot_direction_angle(current_pos:Type[Float64MultiArray], target_pos:Type[Float64MultiArray])->Float64:
    """Calculate direction of robot which it should use to reach target position from current position

    Args:
        current_pos (Type[Float64MultiArray]): current position where the robot is located (approximately)
        target_pos (Type[Float64MultiArray]): target position which should be reached

    Returns:
        Float64: return direction of robot
    """

    #X values identical, but difference in Y values. Determine driving forward or backward
    if current_pos[0] == target_pos[0]:
        if target_pos[1] > current_pos[1]:
            robot_direction = 90.0 #Robot drives forwards
            robot_angle = 90.0
        else:
            robot_direction = 270.0 #Robot drives backwards
            robot_angle = 270.0

    #Y values identical, but difference in X values. Determine driving left or right
    elif current_pos[1] == target_pos[1]:
        if target_pos[0] > current_pos[0]:
            robot_direction = 180.0 #Robot drives to the right
            robot_angle = 0.0
        else:
            robot_direction = 0 #Robot drives to the left
            robot_angle = 180.0

    #target point above
    elif target_pos[1] > current_pos[1]:

        #target point on left upper
        if current_pos[0] > target_pos[0]:

            #arctan(Perpendicular / Base) = angle
            robot_direction = np.arctan(abs(target_pos[1]-current_pos[1])/abs(target_pos[0]-current_pos[0]))*(180.0/np.pi) 
            robot_angle = np.arctan(abs(target_pos[1]-current_pos[1])/abs(target_pos[0]-current_pos[0]))*(180.0/np.pi) + 180.0
        
        #target point on right upper
        else:
            robot_direction = 90.0 + np.arctan((target_pos[1]-current_pos[1])/(target_pos[0]-current_pos[0]))*(180.0/np.pi)
            robot_angle = np.arctan((target_pos[1]-current_pos[1])/(target_pos[0]-current_pos[0]))*(180.0/np.pi)

    #target point below
    else:#target_pos[1] < current_pos[1]:

        #target point on left side
        if current_pos[0] > target_pos[0]:
            robot_direction = 270.0 + (np.arctan(abs(target_pos[1] - current_pos[1])/abs(target_pos[0] - current_pos[0])))*(180.0/np.pi)
            robot_angle = np.arctan((target_pos[1]-current_pos[1])/(target_pos[0]-current_pos[0]))*(180.0/np.pi) - 180.0

        #target point on right side
        else:
            robot_direction = 180.0 + np.arctan(abs(target_pos[1] - current_pos[1])/(target_pos[0] - current_pos[0]))*(180.0/np.pi)
            robot_angle = np.arctan(abs(target_pos[1] - current_pos[1])/(target_pos[0] - current_pos[0]))*(180.0/np.pi)

    return robot_direction, robot_angle

def calc_travel_time(current_pos:Type[Float64MultiArray], target_pos:Type[Float64MultiArray], robot_velocity:Type[Float64])->Float64:
    """Calculate time which is needed to travel from current position to target position

    Args:
        current_pos (Type[Float64MultiArray]): current position where the robot is located (approximately)
        target_pos (Type[Float64MultiArray]): target position which should be reached

    Returns:
        Float64: calculated time
    """
    route_length = (abs(current_pos[0] - target_pos[0])**2 + abs(current_pos[1] - target_pos[1])**2)**(1/2)
    time = route_length / robot_velocity
    return time

def callback():

    #Set parameters robot
    amount_rounds = 1
    robot_velocity = 20
    
    #Driving
    for j in range(amount_rounds):

        for i in range(1,len(trajectory)):
            robot_direction, robot_angle = calc_robot_direction_angle(trajectory[i-1],trajectory[i])
            travel_time = calc_travel_time(trajectory[i],trajectory[i-1],robot_velocity)

            if(i > 1):
                print("\nDriving from position ", trajectory[i-1], " to ", trajectory[i])

            #rospy.sleep(0.5)
            control_input = bytes(f'<{robot_direction},{0},{robot_velocity},{1}>\n', 'utf-8')
            ser.write(control_input)

            #Let the robot drive certain amount of time in direction
            rospy.sleep(travel_time)
            
            #Let the robot stop for a short time
            control_input = bytes(f'<{0},{0},{0},{0}>\n', 'utf-8')
            ser.write(control_input)
            rospy.sleep(3.0)

        control_input = bytes(f'<{0},{0},{0},{0}>\n', 'utf-8')
        ser.write(control_input)
        #rospy.sleep(3.0)


if __name__=="__main__":
    rospy.init_node("trajectory")
    callback()