#!/usr/bin/env python3
from typing import Type
from geometry_msgs.msg import QuaternionStamped
import rospy
from sensorfusion.msg import Position
import numpy as np
import math



class imu(object):

    def __init__(self):

        self.pub_ = rospy.Publisher("/imu_pos_cov", Position, queue_size=1)

        self.pub_angle = rospy.Publisher("/imu_azimuth", Position, queue_size=1)

        rospy.Subscriber("/filter/quaternion", QuaternionStamped, self.callback)

        self.x_vel = 0.0
        self.y_vel = 0.0

 
    def euler_from_quaternion(self,x, y, z, w):
        """
        Convert a quaternion into euler angles (roll, pitch, yaw)
        roll is rotation around x in radians (counterclockwise)
        pitch is rotation around y in radians (counterclockwise)
        yaw is rotation around z in radians (counterclockwise)
        """
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)
     
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)
     
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)
     
        return roll_x, pitch_y, yaw_z # in radians    


    def callback(self,values: Type[QuaternionStamped]) ->None:
        """Read sensordata from topic /imu/dv and publish position values, variances and timestamp to imu_pos_cov

        Args:
            values (type): Message from topic (/imu/dv)
        """
        msg = Position()
        roll_x, pitch_y, yaw_z = self.euler_from_quaternion(values.quaternion.x,values.quaternion.y,values.quaternion.z,values.quaternion.w)
        yaw_z_deg = round(yaw_z * (180.0/np.pi),1)

        if(yaw_z_deg >= -90.0 and yaw_z_deg < 180):
             yaw_z_deg += 90.0
        else:
            yaw_z_deg += 450.0     

        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "/imu_frame"; 
        msg.azimuth = round(yaw_z_deg,2)    

        
        self.pub_angle.publish(msg)
       

        

if __name__ == "__main__":
    rospy.init_node("imu_pos_node", anonymous=True)
    imu_obj = imu()
    rospy.spin()
