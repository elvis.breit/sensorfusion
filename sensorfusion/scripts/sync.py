#!/usr/bin/env python3
import message_filters
import rospy
from sensorfusion.msg import Position, SyncPos
from typing import Type
from sensorfusion.srv import Measurements, MeasurementsResponse


class sync(object):
   
    def __init__(self):        
    
        #self.imu_sub = message_filters.Subscriber("/imu_pos_cov",Position)
        self.lidar_sub = message_filters.Subscriber("/lidar_pos_cov", Position)
        self.vpix_sub = message_filters.Subscriber("/pixart_V_pos_cov", Position)
        self.hpix_sub = message_filters.Subscriber("/pixart_H_pos_cov", Position)

        #Synchronize the sensors
        self.ats = message_filters.ApproximateTimeSynchronizer([self.lidar_sub, self.vpix_sub, self.hpix_sub], queue_size = 5, slop = 0.01)
        self.ats.registerCallback(self.syncedCallback)
        self.service = rospy.Service('getMeasurements_service',Measurements, self.getMeasurements_server)
        self.msg = SyncPos()


    def syncedCallback(self, lidar:Type[Position], pixart_v:Type[Position], pixart_h:Type[Position])->None:
        """sync measurements of sensors and save it to SyncPos msg which contains all data of the sensors at once

        Args:
            lidar (Type[Position]): lidar Position data
            pixart_v (Type[Position]): pixart_v Position data
        """
        self.data_process = True

        #Initialize values to self.msg
        self.msg.header.stamp = pixart_v.header.stamp

        self.msg.x_pos_lid = lidar.x
        self.msg.y_pos_lid = lidar.y
        self.msg.x_var_lid = lidar.x_var
        self.msg.y_var_lid = lidar.y_var

        self.msg.x_pos_vpix = pixart_v.x
        self.msg.y_pos_vpix = pixart_v.y
        self.msg.x_var_vpix = pixart_v.x_var
        self.msg.y_var_vpix = pixart_v.y_var

        self.msg.x_pos_hpix = pixart_h.x
        self.msg.y_pos_hpix = pixart_h.y
        self.msg.x_var_hpix = pixart_h.x_var
        self.msg.y_var_hpix = pixart_h.y_var

        # self.msg.x_pos_imu = imu.x
        # self.msg.y_pos_imu = imu.y
        # self.msg.x_var_imu = imu.x_var
        # self.msg.y_var_imu = imu.y_var

        self.data_process = False

    def getMeasurements_server(self,request):
        """forward SyncPos data to SF_kalman_filter node

        Args:
            request (_type_): without request there occurs an error

        Returns:
            _type_: return SyncPos data
        """
        while(1):
            if(self.data_process == False): break
        
        return MeasurementsResponse(self.msg)
        
if __name__ == "__main__":
    rospy.init_node("synced_measurements")
    sync_obj = sync()
    rospy.spin()



