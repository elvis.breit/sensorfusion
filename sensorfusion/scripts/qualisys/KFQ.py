#!/usr/bin/env python3
from typing import Type
import numpy as np
from sensorfusion.msg import SyncPos, Motion
from sensorfusion.srv import Trajectory, Measurements
import rospy
import csv


class kalman_filter(object):
   
    def __init__(self):        
        
        #*-------User-Parameters--------#

        #Show matrices and states on console
        self.show = False

        #Robot velocity (from 0-100 mm/s)
        self.velocity = 15.0

        #Motion Qualisys on/off
        self.qualisys = True
        
        #*-------Init_Node--------#
        
        #Before shuting down, perform function "save_data" which let the robot stop and saves Position data
        rospy.on_shutdown(self.save_data)

        #Publishes commands to Drive node
        self.pub_ = rospy.Publisher("/drive", Motion, queue_size=3)

        #Synced Position and variances of all sensors with timestamp
        self.Pos = SyncPos()

        #Motion message for giving command to drive to certain direction, velocity and time
        self.motion = Motion()

        #Bool variable for executing functions in first run in specific way
        self.not_init = True

        #Starttime which will be used for noting correct time in csv files after data acquisition
        self.starttime_sec = 0.0

        #Time delay after receiving measurements and calculating while robot still drives forward
        self.start_diff_dt = 0.0
        self.end_diff_dt = 0.0
        self.delay_dt = 0.0
        
        #Save X-Y Position and Timestamp of Kalman Filter and sensors
        self.kf_pos_list = []
        
        self.lid_pos_list = []
        
        self.vpix_pos_list = []

        self.hpix_pos_list = []
        
        self.imu_pos_list = []

        self.qual_pos_list = []

        self.timestamp_list = []

        self.estimate_pos_list = []

        self.vel_dt_list = []
        
        #Robot direction (forward = 90, right = 180, back = 270, left = 0)
        self.direction = 0.0
        
        #Segment number 
        # (example: (0,0)->(30,0) = 1.segment; (30,0)->(30,30) = 2.segment)
        self.seg_number = 1

        #Path number: Small trajectories of one segment 
        # (example: in 1.segment (0,0)->(30,0) are (0,0)->(15,0) 1.path and (15,0)->(30,0) 2. path
        #           in 2.segment (30,0)->(30,30) are (30,0)->(30,15) 3.path and (30,15)->(30,30) 4.path)
        # Intervalls are defined in "trajectory" node
        self.path_number = 1

        #Angle at which robot is driving at current segment
        self.seg_angle = 90.0

        #Angle at which robot is driving at current path
        self.angle = 90.0

        #Time needed driving a segment with specific velocity
        self.seg_dt = 0.0
        
        #Time needed driving a path with specific velocity
        self.dt = 1.0

        #Frequency of self.dt for letting the robot drive after prediction. 
        # After reaching position, it should take measurements after the rate
        self.drive_rate = rospy.Rate(1/self.dt)

        #Driven Distance
        self.driven_distance = (self.velocity*((self.path_number-1)*self.dt))
        
        #*-------Kalman Filter--------# n = 2, m = 6, l = 1

        #state variable (X-Position, Y-Position, X-Velocity, Y-Velocity) init
        # n x 1 column vector
        self.x = np.array([[0],
                          [0]],
                          dtype=float)
        
        #state covariance matrix init
        # n x n matrix
        self.P = np.array([[1,0],
                           [0,1]], 
                          dtype=float)
                
        #measurements
        # m x 1 column vector
        self.z = np.zeros((8,1), dtype=float)
        
        #process noise covariance matrix
        # n x n matrix
        self.Q = np.array([[10,0],
                           [0,10]],
                          dtype=float)
        
        #measurement covariance matrices
        # m x m matrix
        self.R = np.identity(8)

        #state transition matrix
        # n x n matrix
        self.A = np.array([[1,0],
                           [0,1]],
                          dtype=float)
        
        #control-input model
        # n x l matrix
        self.B = np.identity(2)
        
        #control vector
        # l scalar
        self.u = np.array([[round(self.velocity*np.cos(self.angle*np.pi/180),2)],
                          [round(self.velocity*np.sin(self.angle*np.pi/180),2)]],
                          dtype=float)
        
        #process noise
        # n x 1 column vector
        self.w = np.array([[0],
                           [0]],
                          dtype=float)
        
        #state to measurement matrix
        # m x n matrix
        self.H = np.array([[1,0],
                           [0,1],
                           [1,0],
                           [0,1],
                           [1,0],
                           [0,1],
                           [1,0],
                           [0,1]], 
                          dtype=float)
        
        self.HT = np.transpose(self.H)

        #Kalman Gain
        # n x m matrix
        self.K = np.zeros((2,8), dtype=float)

    def save_data(self):
        """let the robot stop and save data to csv files. After that shutdown the node
        """
    
        #Let the robot stop
        self.motion.direction = 0
        self.motion.velocity = 0
        self.motion.dt = 0
        self.motion.move = 0

        self.pub_.publish(self.motion)

        #Write down saved data to csv files
        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/kf_pos.csv', 'w', encoding='UTF8', newline='') as a:
            writer = csv.writer(a)
            writer.writerows(self.kf_pos_list)
        
        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/lidar_pos.csv', 'w', encoding='UTF8', newline='') as b:
            writer = csv.writer(b)
            writer.writerows(self.lid_pos_list)
            
        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/vpix_pos.csv', 'w', encoding='UTF8', newline='') as c:
            writer = csv.writer(c)
            writer.writerows(self.vpix_pos_list)

        # with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/imu_pos.csv', 'w', encoding='UTF8', newline='') as d:
        #     writer = csv.writer(c)
        #     writer.writerows(self.imu_pos_list)

        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/hpix_pos.csv', 'w', encoding='UTF8', newline='') as d:
            writer = csv.writer(d)
            writer.writerows(self.hpix_pos_list)

        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/timestamp.csv', 'w', encoding='UTF8', newline='') as e:
            writer = csv.writer(e)
            writer.writerows(self.timestamp_list)    

        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/estimate_pos.csv', 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(self.estimate_pos_list)

        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/qual_pos.csv', 'w', encoding='UTF8', newline='') as g:
            writer = csv.writer(g)
            writer.writerows(self.qual_pos_list)            

        self.vel_dt_list.append([self.velocity,self.dt])
        with open('/home/pi/catkin_ws/src/sensorfusion/csv_pos/vel_dt.csv', 'w', encoding='UTF8', newline='') as h:
            writer = csv.writer(h)
            writer.writerows(self.vel_dt_list)   
            
    def getPath_client(self,vel,p_num,s_num):
        """Client which request angle, dt and direction to forward the commands to driving server

        Args:
            vel (Type[Float64]): velocity to get the time needed to drive specific path
            p_num (Type[Int64]): number of path of trajectory
        """
        rospy.wait_for_service('getPath_service')
        
        try:
            getPath = rospy.ServiceProxy('getPath_service',Trajectory)
            respond = getPath(p_num,s_num,vel)
            
            self.angle = respond.angle
            if(self.angle > -0.00001 and self.angle < 0.00001): self.angle = 0.00001
            if(self.angle > 179.99999 and self.angle < 180.00001): self.angle = 180.00001
            self.direction = respond.direction
            self.dt = respond.dt
            self.seg_dt = respond.seg_dt
            self.seg_angle = respond.seg_angle

            if(self.angle == 999 and self.direction == 0):
                rospy.signal_shutdown("Finished driving trajectory")
                
            #Setup next pathnumber for next cycle
            self.path_number += 1
        
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def getMeasurements_client(self):
        """get synced measurements (position, variance) of sensors to proceed with update step of kalman filter
        """
        rospy.wait_for_service('getMeasurements_service')
        
        try:
            getMeasurements = rospy.ServiceProxy('getMeasurements_service',Measurements)
            self.Pos = getMeasurements()

        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

    def filter(self):  
        """Kalman Filter function with Prediction and Update Steps
        """
        
        #Variable for execution time
        start = rospy.Time.now().to_sec()

        
        
        #*------Get_Path_Information------#
        
        #Receive necessary information (self.angle, self.dt, self.seg_dt, self.seg_angle)
        self.getPath_client(self.velocity,self.path_number,self.seg_number)
        
        #Update time left to drive to next path position
        if(self.not_init == False):  
            self.end_diff_dt = rospy.Time.now().to_sec()
            self.delay_dt = self.end_diff_dt - self.start_diff_dt
           
  
        
        
        #*------Prediction------#
        
        #angle, vel x, vel y, dt, self.x, self.P, init state, init cov
        if(self.show):
            print("\n\n\n****************New Cycle*****************\n")
            print("angle: ", self.angle)
            print("dt: ", self.dt)
            print("\ninit state: \n", self.x)
            print("\ninit cov: \n", self.P)
            print("\nvel_x: \n", self.u[0][0])
            print("\nvel_y: \n", self.u[1][0])

        # Predict State
        x_p = self.A.dot(self.x) + self.dt*(self.B).dot(self.u) + self.w
        # Predict Covariance 
        P_p = self.A.dot(self.P).dot(np.transpose(self.A)) + self.Q

        self.estimate_pos_list.append([round(x_p[0][0],4), round(x_p[1][0],4)])

        #Predict State, predict covariance
        if(self.show):
            print("\npredict state: \n", x_p)
            print("\npredict cov: \n", P_p)

        #Do one measurement before starting to drive and save data to lists.
        if(self.not_init):
            
            self.getMeasurements_client()
            
            self.starttime_sec = (rospy.Time(self.Pos.measure.header.stamp.secs, self.Pos.measure.header.stamp.nsecs)).to_sec()
            self.timestamp_list.append([0.0])
            self.kf_pos_list.append([0.0, 0.0])
            self.lid_pos_list.append([0.0, 0.0])
            self.vpix_pos_list.append([0.0, 0.0])
            self.hpix_pos_list.append([0.0, 0.0])
            self.qual_pos_list.append([0.0, 0.0])

        
        #*------Drive------#

        #Start driving at first cycle without any more conditions
        if(self.not_init):
            
            self.motion.dt = self.seg_dt
            self.motion.direction = self.direction
            self.motion.velocity = self.velocity
            self.motion.move = 1

            self.pub_.publish(self.motion)
        
        #If angles are quite similar (angles are integers), then command robot to drive another direction
        elif(abs(self.angle - self.seg_angle) < 1.0):
            
            self.motion.dt = self.seg_dt
            self.motion.direction = self.direction
            self.motion.velocity = self.velocity
            self.motion.move = 1

            self.pub_.publish(self.motion)
            print("\n\n******Change Direction to ", round(self.angle,2), " degrees*****\n\n")
            self.seg_number += 1


        #Wait till robot reached position
        rospy.sleep(self.dt-self.delay_dt)

        #*-------Measure--------#
        
        #Receive measurements and save them in self.Pos
        self.getMeasurements_client()               
        self.start_diff_dt = rospy.Time.now().to_sec()
        
        #*--------Update--------#

        # Compute Kalman Gain

        #Observations
        self.z = np.array([[self.Pos.measure.x_pos_lid],
                          [self.Pos.measure.y_pos_lid],
                          [self.Pos.measure.x_pos_vpix],
                          [self.Pos.measure.y_pos_vpix],
                          [self.Pos.measure.x_pos_hpix],
                          [self.Pos.measure.y_pos_hpix],
                          [self.Pos.measure.x_pos_qual],
                          [self.Pos.measure.y_pos_qual]],
                          dtype = float)
        
        #Sensor variances
        var = np.array([[self.Pos.measure.x_var_lid],
                       [self.Pos.measure.y_var_lid],
                       [0.000002*self.driven_distance**2+0.000122*self.driven_distance+0.524632],#self.Pos.measure.x_var_vpix
                       [0.000002*self.driven_distance**2+0.000122*self.driven_distance+0.524632],#self.Pos.measure.y_var_vpix
                       [0.000002*self.driven_distance**2+0.000122*self.driven_distance+0.524632],#self.Pos.measure.x_var_hpix
                       [0.000002*self.driven_distance**2+0.000122*self.driven_distance+0.524632],#self.Pos.measure.y_var_hpix
                       [self.Pos.measure.x_var_qual],
                       [self.Pos.measure.y_var_qual]],
                       dtype = float)
        
        self.R = np.identity(len(self.z)) * (var)   

        #z (measurements), self.R (variances of sensor)
        if(self.show):
            print("\nz: \n", self.z)
            print("R: \n", self.R)

        S = self.H.dot(P_p).dot(self.HT) + self.R 

        try:
            K = P_p.dot(self.HT).dot(np.linalg.inv(S))
        except:
            rospy.signal_shutdown("Singular Matrix, Error due to pixart publisher")

        
        #Kalman Gain
        if(self.show):
            print("\nK: \n", K)
        
        residual = self.z - (self.H.dot(x_p))

        #Estimate state
        self.x = x_p + K.dot(residual)
        
        # Estimate Covariance
        self.P = P_p - K.dot(self.H).dot(P_p)

        
        #*--------Save_values--------#
        
        #Save Data to list
        timestamp_sec = round(((rospy.Time(self.Pos.measure.header.stamp.secs, self.Pos.measure.header.stamp.nsecs)).to_sec()) - self.starttime_sec,4)
        self.timestamp_list.append([timestamp_sec])
        self.kf_pos_list.append([round(self.x[0][0],4), round(self.x[1][0],4)])
        self.lid_pos_list.append([round(self.Pos.measure.x_pos_lid,4), round(self.Pos.measure.y_pos_lid,4)])
        self.vpix_pos_list.append([round(self.Pos.measure.x_pos_vpix,4), round(self.Pos.measure.y_pos_vpix,4)])
        self.hpix_pos_list.append([round(self.Pos.measure.x_pos_hpix,4), round(self.Pos.measure.y_pos_hpix,4)])
        self.qual_pos_list.append([round(self.Pos.measure.x_pos_qual,4), round(self.Pos.measure.y_pos_qual,4)])
        #self.imu_pos_list.append([round(self.Pos.measure.x_pos_imu,4), round(self.Pos.measure.y_pos_imu,4)])
        
        #*---------Output Screen------#
        #print("\nKF-Position: [" ,round(self.x[0][0],4), ", ", round(self.x[1][0],4),"]\n")

        if(self.not_init):
            self.not_init = False

        #Execution time
        end = rospy.Time.now().to_sec()
        #print("Execution time: ", round((end-start)-(self.dt-self.delay_dt),4)," sec\n")
        



if __name__ == "__main__":
    rospy.init_node("kalman_filter", anonymous=True, disable_signals=True)
    kf_obj = kalman_filter()
    
    #wait till all other nodes launched and Pixart Publisher restarted when errors occured, till starting using kalman filter
    rospy.sleep(9.5) 
    
    while not rospy.is_shutdown():
        kf_obj.filter()

