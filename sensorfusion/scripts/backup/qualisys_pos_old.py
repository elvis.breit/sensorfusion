#!/usr/bin/env python3
from typing import Type
from std_msgs.msg import Float64MultiArray
import rospy
from sensorfusion.msg import Position
import numpy as np


def callback(values: Type[Float64MultiArray]) ->None:
    """Read sensordata from topic qualisys_pos and publish position values, variances and timestamp to qualisys_pos_cov

    Args:
        values (Type[Float64MultiArray]): Message from topic (qualisys_pos)
    """
    pub_ = rospy.Publisher("/qualisys_pos_cov", Position, queue_size=1)
    
    msg = Position()
    msg.x = np.round(values.data[0],3)
    msg.y = np.round(values.data[1],3) 
    msg.x_var = values.data[2]**2 
    msg.y_var = values.data[2]**2
    msg.header.stamp = rospy.Time.now()
    msg.header.frame_id = "/qualisys_frame";       
    pub_.publish(msg)

def listener() -> None:
    """Initialize node and subscribe to topic 'qualisys_pos'"""
    rospy.init_node("qualisys_pos_node", anonymous=True)
    rospy.Subscriber("qualisys_pub", Float64MultiArray, callback)
    rospy.spin()

if __name__ == "__main__":
    listener()
