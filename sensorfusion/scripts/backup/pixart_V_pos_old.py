#!/usr/bin/env python3
from typing import Type
from std_msgs.msg import Float64MultiArray
import rospy
from sensorfusion.msg import Position
import numpy as np


def callback(values: Type[Float64MultiArray]) ->None:
    """Read sensordata from topic PAA5102_V_values and publish position values, variances and timestamp to pixart_pos_cov

    Args:
        values (Type[Float64MultiArray]): Message from topic (PAA5102_V_values)
    """
    msg.x = np.round(values.data[0] / -11.803, 3) #Umrechnungsfaktor X-Position
    msg.y = np.round(values.data[1] / 11.966, 3) #Umrechnungsfaktor Y-Position
    msg.x_var = 6.25 #TODO estimate variance
    msg.y_var = 6.25 
    msg.header.stamp = rospy.Time.now()
    msg.header.frame_id = "/pixart_V_frame";       
    pub_.publish(msg)
    

if __name__ == "__main__":
    rospy.init_node("pixart_V_pos", anonymous=True)
    rospy.Subscriber("PAA5102_V_values", Float64MultiArray, callback)
    pub_ = rospy.Publisher("/pixart_V_pos_cov", Position, queue_size=1)
    msg = Position()
    rospy.spin()