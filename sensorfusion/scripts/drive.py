#!/usr/bin/env python3
from typing import Type
import rospy
import serial
from sensorfusion.msg import Motion


#Open Serial to create connection to motor drivers
ser = serial.Serial(
		'/dev/ttyS0', 
		baudrate = 115200,
		parity = serial.PARITY_NONE,
		stopbits=serial.STOPBITS_ONE,
		bytesize =serial.EIGHTBITS,
		timeout = 1
		)

ser.close()
ser.open()

#Let the robot stop, to be sure it isn't driving after last run
ser.write(bytes(f'<{0},{0},{0},{0}>\n', 'utf-8'))


class drive(object):

    def __init__(self):        

        #Subscribe to topic /drive and listen to published messages of kalman filter node
        self.sub_ = rospy.Subscriber("/drive", Motion, self.drive)

        
    def drive(self,values):
        """callback after receiving message of topic /drive. Drive with commands given by kalman filter

        Args:
            values (direction, velocity, move and dt)
        """
        #Let the robot drive with submitted commands of kalman filter node
        #(robot_direction(0-359), rotation(??), robot_velocity (0-100), start=1||stop=0)

        control_input = bytes(f'<{round(values.direction,2)},{0},{values.velocity},{values.move}>\n', 'utf-8')
        print(control_input)
        ser.write(control_input)
        
        #Drive dt amount of time
        rospy.sleep(values.dt)

        #Stop the robot after it has driven dt time
        control_input = bytes(f'<{0},{0},{0},{0}>\n', 'utf-8')
        ser.write(control_input)
        

if __name__=="__main__":
    rospy.init_node("drive_node")
    drive_obj = drive()
    rospy.spin()
