import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import uuid
import csv
from itertools import chain

plot_on_windows = True

#Read out data from CSV files
if(plot_on_windows):
    
    v10_1_raw = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\Bogen\\v10_bogen_1.csv', header=None)
    v10_2_raw = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\Bogen\\v10_bogen_2.csv', header=None)
    v50_1_raw = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\Bogen\\v50_bogen_1.csv', header=None)
    v50_2_raw= pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\Bogen\\v50_bogen_2.csv', header=None)
    v100_1_raw = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\Bogen\\v100_bogen_1.csv', header=None)
    v100_2_raw = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\Bogen\\v100_bogen_2.csv', header=None)
    
       
#Create Arrays for each position
v10_1_x = []
v10_1_y = []
v50_1_x = []
v50_1_y = []
v100_1_x = []
v100_1_y = []

v10_2_x = []
v10_2_y = []
v50_2_x = []
v50_2_y = []
v100_2_x = []
v100_2_y = []


for i in range(len(v10_1_raw)):
    v10_1_x.append([float(v10_1_raw[1][i])])
    v50_1_x.append([float(v50_1_raw[1][i])])
    v100_1_x.append([float(v100_1_raw[1][i])])
    
    v10_1_y.append([float(v10_1_raw[3][i])])
    v50_1_y.append([float(v50_1_raw[3][i])])
    v100_1_y.append([float(v100_1_raw[3][i])])
    
    v10_2_x.append([float(v10_2_raw[1][i])])
    v50_2_x.append([float(v50_2_raw[1][i])])
    v100_2_x.append([float(v100_2_raw[1][i])])
    
    v10_2_y.append([float(v10_2_raw[3][i])])
    v50_2_y.append([float(v50_2_raw[3][i])])
    v100_2_y.append([float(v100_2_raw[3][i])])
      

v10_1_x_diff = np.diff(np.array(v10_1_x).reshape(-1))
v10_1_y_diff = np.diff(np.array(v10_1_y).reshape(-1))
v10_2_x_diff = np.diff(np.array(v10_2_x).reshape(-1))
v10_2_y_diff = np.diff(np.array(v10_2_y).reshape(-1))
v10_x_diff = list(chain(v10_1_x_diff,v10_2_x_diff))
v10_y_diff = list(chain(v10_1_y_diff,v10_2_y_diff))

v50_1_x_diff = np.diff(np.array(v50_1_x).reshape(-1))
v50_1_y_diff = np.diff(np.array(v50_1_y).reshape(-1))
v50_2_x_diff = np.diff(np.array(v50_2_x).reshape(-1))
v50_2_y_diff = np.diff(np.array(v50_2_y).reshape(-1))
v50_x_diff = list(chain(v50_1_x_diff,v50_2_x_diff))
v50_y_diff = list(chain(v50_1_y_diff,v50_2_y_diff))

v100_1_x_diff = np.diff(np.array(v100_1_x).reshape(-1))
v100_1_y_diff = np.diff(np.array(v100_1_y).reshape(-1))
v100_2_x_diff = np.diff(np.array(v100_2_x).reshape(-1))
v100_2_y_diff = np.diff(np.array(v100_2_y).reshape(-1))
v100_x_diff = list(chain(v100_1_x_diff,v100_2_x_diff))
v100_y_diff = list(chain(v100_1_y_diff,v100_2_y_diff))

for i in range(len(v10_x_diff)):
    v10_x_diff[i] -= 119.497
    v50_x_diff[i] -= 119.497
    v100_x_diff[i] -= 119.497
    v10_y_diff[i] -= 119.497
    v50_y_diff[i] -= 119.497
    v100_y_diff[i] -= 119.497


APv10 = np.max([((np.mean(v10_x_diff))**2 + ((np.mean(v10_y_diff)))**2)**(1/2)])
APv50 = np.max([((np.mean(v50_x_diff))**2 + ((np.mean(v50_y_diff)))**2)**(1/2)])
APv100 = np.max([((np.mean(v100_x_diff))**2 + ((np.mean(v100_y_diff)))**2)**(1/2)])



print("\nX-Position Differenz Durchschnitt")
print("******************************")
print("v10_x_diff = ",np.mean(v10_x_diff))
print("v50_x_diff = ",np.mean(v50_x_diff))
print("v100_x_diff = ",np.mean(v100_x_diff))

print("\nY-Position Differenz Durchschnitt")
print("******************************")
print("v10_y_diff = ",np.mean(v10_y_diff))
print("v50_y_diff = ",np.mean(v50_y_diff))
print("v100_y_diff = ",np.mean(v100_y_diff))

print("\nPose-Genauigkeit")
print("*******************")
print("APv10 = ",APv10)
print("APv50 = ",APv50)
print("APv100 = ",APv100)

e10 = (210+APv10)/(210/10) - 10
e50 = (210+APv50)/(210/50) - 50
e100 = (210+APv100)/(210/100) - 100

print("\nDifferenz gemessene und angegebene Geschwindigkeit")
print("*************************")
print("e10 = ",e10)
print("e50 = ",e50)
print("e100 = ",e100)

err10 = e10/10
err50 = e50/50
err100 = e100/100

print("\nProzentualer Fehler")
print("*******************")
print("err10 = ",round(err10*100,3),"%")
print("err50 = ",round(err50*100,3),"%")
print("err100 = ",round(err100*100,3),"%")


plt.rc('axes', axisbelow=True)

X = ['1','2','3','4', '5',
     '6','7','8', '9','10']
X_axis = np.arange(len(X))

plot1 = plt.figure(1)
plt.grid(True,axis = 'y')
plt.bar(X_axis - 0.2, v10_x_diff, 0.3, label = 'X',color='steelblue')
plt.bar(X_axis + 0.2, v10_y_diff, 0.3, label = 'Y',color='lightsteelblue')

plt.xticks(X_axis, X)
plt.xlabel("Zyklus")
plt.ylabel("Abweichung [$mm$]")
plt.title("Abweichungen Kreisbahn - v10",fontweight="bold")
plt.legend()




plot2 = plt.figure(2)
plt.grid(True,axis = 'y')
plt.bar(X_axis - 0.2, v50_x_diff, 0.3, label = 'X',color='steelblue')
plt.bar(X_axis + 0.2, v50_y_diff, 0.3, label = 'Y',color='lightsteelblue')

plt.xticks(X_axis, X)
plt.xlabel("Zyklus")
plt.ylabel("Abweichung [$mm$]")
plt.title("Abweichungen Kreisbahn - v50",fontweight="bold")
plt.legend()



plot3 = plt.figure(3)
plt.grid(True,axis = 'y')
plt.bar(X_axis - 0.2, v100_x_diff, 0.3, label = 'X',color='steelblue')
plt.bar(X_axis + 0.2, v100_y_diff, 0.3, label = 'Y',color='lightsteelblue')

plt.xticks(X_axis, X)
plt.xlabel("Zyklus")
plt.ylabel("Abweichung [$mm$]")
plt.title("Abweichungen Kreisbahn - v100",fontweight="bold")
plt.legend()



plot4 = plt.figure(4)
X = ['10','50','100']
X_axis = np.arange(len(X))
plt.xticks(X_axis, X)
plt.grid(True,axis = 'y')
plt.bar(X_axis,[APv10,APv50,APv100], label = 'Pose-Genauigkeit',color='slategrey',width = 0.5)
plt.xlabel("Geschwindigkeit [$mm/s$]")
plt.ylabel("Pose-Genauigkeit [$mm$]")
plt.title("Pose-Genauigkeit Kreisbahn",fontweight="bold")
plt.legend()

plt.show()