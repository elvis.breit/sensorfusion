import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import uuid
import csv



def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

#Read out data from CSV files
traj = pd.read_csv('C:\\Users\\user\\Desktop\\csv_path\\path.csv', header=None)
kf = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\kf_pos.csv', header=None)
hpix = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\hpix_pos.csv', header=None)
vpix = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\vpix_pos.csv', header=None)
lid = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\lidar_pos.csv', header=None)
qual = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\qual_pos.csv', header=None)
time = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\timestamp.csv', header=None)
est = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\estimate_pos.csv', header=None)
vel_dt = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\vel_dt.csv', header=None)
#leica = pd.read_csv('C:\\Users\\user\\Desktop\\UDPMonitorLog.csv', header=None)
leica = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v100\\3\\UDPMonitorLog.csv',header=None)  

    

leica_frequency = 250

def get_leica_data(leica,freq):

 #Get Position data 
    # original: X,[X-Position],Y,[Y-Position],Z,[Z-Position],Time
    # converted: [X_position][Y_Position][Z_Position]
    leica_raw_data = []
    
    for i in range(len(leica)):
        leica_raw_data.append([float(leica[1][i]),float(leica[3][i]),float(leica[7][i])])

    #Check how many samples of Lasertrackers are done in the range of the sample time of Kalman filter
    #Save the result in compare_dt
    len_raw = len(leica_raw_data)
    compare_dt = []

    for i in range(len_raw):
        if((leica_raw_data[i][2]) - (leica_raw_data[0][2]) < vel_dt[1][0]):
            continue
        else:
            compare_dt.append(i)
            break


    #Necessary to determine at which time the robot starts to move. Prior measurements of lasertracker are not relevant.
    #Therefore we compare the measurements with the index difference of compare_dt and state that a change of 0.05mm in x or y direction 
    #can be regarded as movement of the robot and relevant measurements of lasertracker
    start_index = []

    for j in range(compare_dt[0],len_raw):
        if((leica_raw_data[j][0] - leica_raw_data[j-compare_dt[0]][0] >= 0.05) or (leica_raw_data[j][1] - leica_raw_data[j-compare_dt[0]][1] >= 0.05)):
            start_index.append(j-3)
            break
        else: 
            continue

    #Adjust coordinate system to robots by using first measurement and subtract it from the next ones
    leica_data = []

    for i in range(start_index[0],len(leica)):
        leica_data.append([((leica_raw_data[i][0])-(leica_raw_data[start_index[0]][0])),((leica_raw_data[i][1])-(leica_raw_data[start_index[0]][1])),((leica_raw_data[i][2])-(leica_raw_data[start_index[0]][2]))])

    duration = []
    duration.append(time[0][len(time)-1])

    leica_data_temp = []
    for i in range(len(leica_data)):
        if(leica_data[i][2] <= duration[0]):
            leica_data_temp.append([leica_data[i][0],leica_data[i][1],leica_data[i][2]])

    leica_data = leica_data_temp

    # #Filter lasertracker measurements by using only data which correspond to KF timestamps 
    # useful_leica_data = []

    # time_len = len(time)

    # for i in range(time_len):
    #     for j in range(len(leica_data)):
    #         if(abs(leica_data[j][2] - time[0][i]) <= 1/leica_frequency):
    #             useful_leica_data.append([leica_data[j][0],leica_data[j][1]])
    #             break

    #Filter lasertracker measurements by using only data which correspond to KF timestamps 

    #Write results to csv file and save data to array
    with open('C:\\Users\\user\\Desktop\\leica_pos.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(leica_data)
    
    leica_temp = pd.read_csv('C:\\Users\\user\\Desktop\\leica_pos.csv',header=None)        



    distances = []
    useful_leica_data = []
    min_dist_index = []
    amount_pos = len(kf)

    for i in range(amount_pos):
        for j in range(len(leica_data)):
            distances.append([((leica_temp[0][j] - kf[0][i])**2 + (leica_temp[1][j] - kf[1][i])**2)**(1/2),j])
        min_dist_index.append(min(distances))
        distances = []


    for i in range(len(min_dist_index)):
        useful_leica_data.append(leica_data[min_dist_index[i][1]])
        
    #Write results to csv file and save data to array
    with open('C:\\Users\\user\\Desktop\\leica_pos.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(useful_leica_data)
    
    leica = pd.read_csv('C:\\Users\\user\\Desktop\\leica_pos.csv',header=None)  
                
    return leica

array = get_leica_data(leica,leica_frequency)
print(array[0])