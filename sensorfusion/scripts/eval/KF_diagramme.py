import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import uuid
import csv
import statistics

plot_on_windows = True
leica_frequency = 250


def rmse(predictions, targets):
    #return np.sqrt(((predictions - targets) ** 2).mean())
    return np.sqrt(np.mean(((predictions - targets) ** 2)))

def get_leica_data(leica,freq):


      #Get Position data 
    # original: X,[X-Position],Y,[Y-Position],Z,[Z-Position],Time
    # converted: [X_position][Y_Position][Z_Position]
    leica_raw_data = []
    
    for i in range(len(leica)):
        leica_raw_data.append([float(leica[1][i]),float(leica[3][i]),float(leica[7][i])])

    #Check how many samples of Lasertrackers are done in the range of the sample time of Kalman filter
    #Save the result in compare_dt
    len_raw = len(leica_raw_data)
    compare_dt = []

    for i in range(len_raw):
        if((leica_raw_data[i][2]) - (leica_raw_data[0][2]) < vel_dt[1][0]):
            continue
        else:
            compare_dt.append(i)
            break


    #Necessary to determine at which time the robot starts to move. Prior measurements of lasertracker are not relevant.
    #Therefore we compare the measurements with the index difference of compare_dt and state that a change of 0.05mm in x or y direction 
    #can be regarded as movement of the robot and relevant measurements of lasertracker
    start_index = []

    for j in range(compare_dt[0],len_raw):
        if((leica_raw_data[j][0] - leica_raw_data[j-compare_dt[0]][0] >= 0.05) or (leica_raw_data[j][1] - leica_raw_data[j-compare_dt[0]][1] >= 0.05)):
            start_index.append(j-3)
            break
        else: 
            continue

    #Adjust coordinate system to robots by using first measurement and subtract it from the next ones
    leica_data = []

    for i in range(start_index[0],len(leica)):
        leica_data.append([((leica_raw_data[i][0])-(leica_raw_data[start_index[0]][0])),((leica_raw_data[i][1])-(leica_raw_data[start_index[0]][1])),((leica_raw_data[i][2])-(leica_raw_data[start_index[0]][2]))])

    duration = []
    duration.append(time[0][len(time)-1])

    leica_data_temp = []
    for i in range(len(leica_data)):
        if(leica_data[i][2] <= duration[0]):
            leica_data_temp.append([leica_data[i][0],leica_data[i][1],leica_data[i][2]])

    leica_data = leica_data_temp

    # #Filter lasertracker measurements by using only data which correspond to KF timestamps 
    # useful_leica_data = []

    # time_len = len(time)

    # for i in range(time_len):
    #     for j in range(len(leica_data)):
    #         if(abs(leica_data[j][2] - time[0][i]) <= 1/leica_frequency):
    #             useful_leica_data.append([leica_data[j][0],leica_data[j][1]])
    #             break

    #Filter lasertracker measurements by using only data which correspond to KF timestamps 

    #Write results to csv file and save data to array
    with open('C:\\Users\\user\\Desktop\\leica_pos.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(leica_data)
    
    leica_temp = pd.read_csv('C:\\Users\\user\\Desktop\\leica_pos.csv',header=None)        



    distances = []
    useful_leica_data = []
    min_dist_index = []
    amount_pos = len(kf)

    for i in range(amount_pos):
        for j in range(len(leica_data)):
            distances.append([((leica_temp[0][j] - kf[0][i])**2 + (leica_temp[1][j] - kf[1][i])**2)**(1/2),j])
        min_dist_index.append(min(distances))
        distances = []


    for i in range(len(min_dist_index)):
        useful_leica_data.append(leica_data[min_dist_index[i][1]])
        
    #Write results to csv file and save data to array
    with open('C:\\Users\\user\\Desktop\\leica_pos.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(useful_leica_data)
    
    leica = pd.read_csv('C:\\Users\\user\\Desktop\\leica_pos.csv',header=None)  
                
    return leica

#Read out data from CSV files
if(plot_on_windows):
    
    traj = pd.read_csv('C:\\Users\\user\\Desktop\\csv_path\\path.csv', header=None)
    kf = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\kf_pos.csv', header=None)
    hpix = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\hpix_pos.csv', header=None)
    vpix = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\vpix_pos.csv', header=None)
    lid = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\lidar_pos.csv', header=None)
    qual = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\qual_pos.csv', header=None)
    time = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\timestamp.csv', header=None)
    est = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\estimate_pos.csv', header=None)
    vel_dt = pd.read_csv('C:\\Users\\user\\Desktop\\csv_pos\\vel_dt.csv', header=None)
    #leica = pd.read_csv('D:\\Messungen\\UDPMonitorLog.csv',header=None)  
    leica = pd.read_csv('C:\\Users\\user\\Desktop\\UDPMonitorLog.csv',header=None)  
    
else:
    traj = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_path/path.csv', header=None)
    kf = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/kf_pos.csv', header=None)
    hpix = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/hpix_pos.csv', header=None)
    vpix = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/vpix_pos.csv', header=None)
    lid = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/lidar_pos.csv', header=None)
    qual = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/qual_pos.csv', header=None)
    time = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/timestamp.csv', header=None)
    est = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/estimate_pos.csv', header=None)
    vel_dt = pd.read_csv('/home/pi/catkin_ws/src/sensorfusion/csv_pos/vel_dt.csv', header=None)

leica_pos = get_leica_data(leica,leica_frequency)

print(leica_pos[0])

    
plt.rc('axes', axisbelow=True)
    
#Position Data On Each Measurement Update
plot1 = plt.figure(1)
plt.suptitle('Positionen Prüfbahn\n', fontweight="bold")
plt.title('Geschwindigkeit: %i' %int(vel_dt[0][0]) + ' mm/s')
plt.xlabel('X-Position [$mm$]')
plt.ylabel('Y-Position [$mm$]')
plt.grid(True)

#ax = plt.gca()
#ax.set_aspect('equal', adjustable='box')

plt.plot(traj[0],traj[1],'k--',label='Ideale Trajektorie')
plt.scatter(vpix[0],vpix[1],label='Pixart_front')
plt.scatter(hpix[0],hpix[1],label='Pixart_back')
plt.scatter(lid[0],lid[1],label='LiDAR')
#plt.scatter(qual[0],qual[1],label='Qualisys',color='magenta')
plt.scatter(est[0],est[1],label='Schätzung')
plt.plot(kf[0],kf[1],color='red',label='Kalman Filter',ls='--')
plt.scatter(kf[0],kf[1],color='red')
plt.scatter(leica_pos[0],leica_pos[1],marker='X',color='blue')
plt.plot(leica_pos[0],leica_pos[1],label='Leica', ls='--',color='blue')

plt.legend(fontsize=8)

plt.savefig(r"C:/Users/user/Desktop/Results/" + str(uuid.uuid4()) + r".png")


#X-Position Errors On Each Measurement Update
kf_x_diff = np.subtract(kf[0],leica_pos[0])
vpix_x_diff = np.subtract(vpix[0],leica_pos[0])
hpix_x_diff = np.subtract(hpix[0],leica_pos[0])
lid_x_diff = np.subtract(lid[0],leica_pos[0])
#qual_x_diff = np.subtract(qual[0],leica_pos[0])
est_x_diff = np.subtract(est[0],leica_pos[0])

# print(len(kf))
# print(len(leica_pos))

plot2 = plt.figure(2)
plt.suptitle('X-Position Abweichung\n', fontweight="bold")
plt.title('RMSE: %f' %rmse(kf[0],leica_pos[0]) + ' mm')

plt.xlabel('Zeit [$s$]')
plt.ylabel('X-Abweichung [$mm$]')
plt.grid(True)

plt.plot(time,np.zeros(len(time)),'--',label='Leica Trajektorie',color='blue')
plt.scatter(time,vpix_x_diff,label='Pixart_front')
plt.scatter(time,hpix_x_diff,label='Pixart_back')
plt.scatter(time,lid_x_diff,label='LiDAR')
plt.scatter(time,est_x_diff,label='Schätzung')

#plt.scatter(time,qual_x_diff,label='Qualisys',color='magenta')
plt.plot(time,kf_x_diff,color='red',label='Kalman Filter',ls='--')
plt.scatter(time,kf_x_diff,color='red')

plt.legend(fontsize=10)

plt.savefig(r"C:/Users/user/Desktop/Results/" + str(uuid.uuid4()) + r".png")




#Y-Position Errors On Each Measurement Update
kf_y_diff = np.subtract(kf[1],leica_pos[1])
vpix_y_diff = np.subtract(vpix[1],leica_pos[1])
hpix_y_diff = np.subtract(hpix[1],leica_pos[1])
lid_y_diff = np.subtract(lid[1],leica_pos[1])
#qual_y_diff = np.subtract(qual[1],leica_pos[1])
est_y_diff = np.subtract(est[1],leica_pos[1])


plot3 = plt.figure(3)
#plt.title('Y-Position Abweichung\n\nRMSE: %f\n' %rmse(kf[1],leica_pos[1]), fontweight="bold")

plt.suptitle('Y-Position Abweichung\n', fontweight="bold")
plt.title('RMSE: %f' %rmse(kf[1],leica_pos[1]) + ' mm')

plt.xlabel('Zeit [$s$]')
plt.ylabel('Y-Abweichung [$mm$]')
plt.grid(True)

plt.plot(time,np.zeros(len(time)),'--',label='Leica Trajektorie',color='blue')
plt.scatter(time,vpix_y_diff,label='Pixart_front')
plt.scatter(time,hpix_y_diff,label='Pixart_back')
plt.scatter(time,lid_y_diff,label='LiDAR')
plt.scatter(time,est_y_diff,label='Schätzung')
#plt.scatter(time,qual_y_diff,label='Qualisys',color='magenta')
plt.plot(time,kf_y_diff,color='red',label='Kalman Filter',ls='--')
plt.scatter(time,kf_y_diff,color='red')

plt.legend(fontsize=10)

plt.savefig(r"C:/Users/user/Desktop/Results/" + str(uuid.uuid4()) + r".png")


plt.show()