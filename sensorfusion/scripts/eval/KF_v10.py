import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import uuid
import csv

plot_on_windows = True
leica_frequency = 210

vel_dt = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\1\\csv_pos\\vel_dt.csv', header=None)

#Read out data from CSV files
kf_1 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\1\\csv_pos\\kf_pos.csv', header=None)
leica_1 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\1\\UDPMonitorLog.csv',header=None)
time_1 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\1\\csv_pos\\timestamp.csv', header=None)  

kf_2 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\2\\csv_pos\\kf_pos.csv', header=None)
leica_2 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\2\\UDPMonitorLog.csv',header=None)
time_2 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\2\\csv_pos\\timestamp.csv', header=None)   

kf_3 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\3\\csv_pos\\kf_pos.csv', header=None)
leica_3 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\3\\UDPMonitorLog.csv',header=None) 
time_3 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\3\\csv_pos\\timestamp.csv', header=None)  

kf_4 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\4\\csv_pos\\kf_pos.csv', header=None)
leica_4 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\4\\UDPMonitorLog.csv',header=None) 
time_4 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\4\\csv_pos\\timestamp.csv', header=None)  

kf_5 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\5\\csv_pos\\kf_pos.csv', header=None)
leica_5 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\5\\UDPMonitorLog.csv',header=None)  
time_5 = pd.read_csv('C:\\Users\\user\\Documents\\MEGA\\Studium\\Thesis\\Results\\KF\\v10\\5\\csv_pos\\timestamp.csv', header=None) 



def rmse(predictions, targets):
    return np.sqrt(np.mean(((predictions - targets) ** 2)))

def get_leica_data(leica,time,kf):


    #Get Position data 
    # original: X,[X-Position],Y,[Y-Position],Z,[Z-Position],Time
    # converted: [X_position][Y_Position][Z_Position]
    leica_raw_data = []
    
    for i in range(len(leica)):
        leica_raw_data.append([float(leica[1][i]),float(leica[3][i]),float(leica[7][i])])

    #Check how many samples of Lasertrackers are done in the range of the sample time of Kalman filter
    #Save the result in compare_dt
    len_raw = len(leica_raw_data)
    compare_dt = []

    for i in range(len_raw):
        if((leica_raw_data[i][2]) - (leica_raw_data[0][2]) < vel_dt[1][0]):
            continue
        else:
            compare_dt.append(i)
            break


    #Necessary to determine at which time the robot starts to move. Prior measurements of lasertracker are not relevant.
    #Therefore we compare the measurements with the index difference of compare_dt and state that a change of 0.05mm in x or y direction 
    #can be regarded as movement of the robot and relevant measurements of lasertracker
    start_index = []

    for j in range(compare_dt[0],len_raw):
        if((leica_raw_data[j][0] - leica_raw_data[j-compare_dt[0]][0] >= 0.05) or (leica_raw_data[j][1] - leica_raw_data[j-compare_dt[0]][1] >= 0.05)):
            start_index.append(j-3)
            break
        else: 
            continue

    #Adjust coordinate system to robots by using first measurement and subtract it from the next ones
    leica_data = []

    for i in range(start_index[0],len(leica)):
        leica_data.append([((leica_raw_data[i][0])-(leica_raw_data[start_index[0]][0])),((leica_raw_data[i][1])-(leica_raw_data[start_index[0]][1])),((leica_raw_data[i][2])-(leica_raw_data[start_index[0]][2]))])

    duration = []
    duration.append(time[0][len(time)-1])

    leica_data_temp = []
    for i in range(len(leica_data)):
        if(leica_data[i][2] <= duration[0]):
            leica_data_temp.append([leica_data[i][0],leica_data[i][1],leica_data[i][2]])

    leica_data = leica_data_temp

    #Write results to csv file and save data to array
    with open('C:\\Users\\user\\Desktop\\leica_pos.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(leica_data)
    
    leica_temp = pd.read_csv('C:\\Users\\user\\Desktop\\leica_pos.csv',header=None)        



    distances = []
    useful_leica_data = []
    min_dist_index = []
    amount_pos = len(kf)

    for i in range(amount_pos):
        for j in range(len(leica_data)):
            distances.append([((leica_temp[0][j] - kf[0][i])**2 + (leica_temp[1][j] - kf[1][i])**2)**(1/2),j])
        min_dist_index.append(min(distances))
        distances = []


    for i in range(len(min_dist_index)):
        useful_leica_data.append(leica_data[min_dist_index[i][1]])
        
    #Write results to csv file and save data to array
    with open('C:\\Users\\user\\Desktop\\leica_pos.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(useful_leica_data)
    
    leica = pd.read_csv('C:\\Users\\user\\Desktop\\leica_pos.csv',header=None)  
                
    return leica


leica_pos_1 = get_leica_data(leica_1,time_1,kf_1)
kf_x_1 = []
kf_y_1 = []
leica_x_1 = []
leica_y_1 = []

leica_pos_2 = get_leica_data(leica_2,time_2,kf_2)
kf_x_2 = []
kf_y_2 = []
leica_x_2 = []
leica_y_2 = []

leica_pos_3 = get_leica_data(leica_3,time_3,kf_3)
kf_x_3 = []
kf_y_3 = []
leica_x_3 = []
leica_y_3 = []

leica_pos_4 = get_leica_data(leica_4,time_4,kf_4)
kf_x_4 = []
kf_y_4 = []
leica_x_4 = []
leica_y_4 = []


leica_pos_5 = get_leica_data(leica_5,time_5,kf_5)
kf_x_5 = []
kf_y_5 = []
leica_x_5 = []
leica_y_5 = []



for i in range(len(kf_1)):
    kf_x_1.append(kf_1[0][i])
    kf_y_1.append(kf_1[1][i])
    kf_x_2.append(kf_2[0][i])
    kf_y_2.append(kf_2[1][i])
    kf_x_3.append(kf_3[0][i])
    kf_y_3.append(kf_3[1][i])
    kf_x_4.append(kf_4[0][i])
    kf_y_4.append(kf_4[1][i])
    kf_x_5.append(kf_5[0][i])
    kf_y_5.append(kf_5[1][i])
    
    leica_x_1.append(leica_pos_1[0][i])
    leica_y_1.append(leica_pos_1[1][i])
    leica_x_2.append(leica_pos_2[0][i])
    leica_y_2.append(leica_pos_2[1][i])
    leica_x_3.append(leica_pos_3[0][i])
    leica_y_3.append(leica_pos_3[1][i])
    leica_x_4.append(leica_pos_4[0][i])
    leica_y_4.append(leica_pos_4[1][i])
    leica_x_5.append(leica_pos_5[0][i])
    leica_y_5.append(leica_pos_5[1][i])
    
    
kf_x = np.transpose([kf_x_1,kf_x_2,kf_x_3,kf_x_4,kf_x_5])
leica_x = np.transpose([leica_x_1,leica_x_2,leica_x_3,leica_x_4,leica_x_5])
kf_y = np.transpose([kf_y_1,kf_y_2,kf_y_3,kf_y_4,kf_y_5])
leica_y = np.transpose([leica_y_1,leica_y_2,leica_y_3,leica_y_4,leica_y_5])

rmse_x_1 = []
rmse_y_1 = []
rmse_x_2 = []
rmse_y_2 = []
rmse_x_3 = []
rmse_y_3 = []
rmse_x_4 = []
rmse_y_4 = []
rmse_x_5 = []
rmse_y_5 = []
 

rmse_x_1 = rmse((kf_1[0]),(leica_pos_1[0]))
rmse_y_1 = rmse((kf_1[1]),(leica_pos_1[1]))
rmse_x_2 = rmse((kf_2[0]),(leica_pos_2[0]))
rmse_y_2 = rmse((kf_2[1]),(leica_pos_2[1]))
rmse_x_3 = rmse((kf_3[0]),(leica_pos_3[0]))
rmse_y_3 = rmse((kf_3[1]),(leica_pos_3[1]))
rmse_x_4 = rmse((kf_4[0]),(leica_pos_4[0]))
rmse_y_4 = rmse((kf_4[1]),(leica_pos_4[1]))
rmse_x_5 = rmse((kf_5[0]),(leica_pos_5[0]))
rmse_y_5 = rmse((kf_5[1]),(leica_pos_5[1]))

rmse_x = [rmse_x_1,rmse_x_2,rmse_x_3,rmse_x_4,rmse_x_5]
rmse_y = [rmse_y_1,rmse_y_2,rmse_y_3,rmse_y_4,rmse_y_5]


print(rmse_x,rmse_y)


kf_mean_x = []
leica_mean_x = []
kf_mean_y = []
leica_mean_y = []


for i in range(len(kf_x)):
    kf_mean_x.append(round(np.mean(kf_x[i]),4))
    leica_mean_x.append(round(np.mean(leica_x[i]),4))
    kf_mean_y.append(round(np.mean(kf_y[i]),4))
    leica_mean_y.append(round(np.mean(leica_y[i]),4))

#Bahn-Genauigkeit
Bahn_Genauigkeit = []

for i in range(len(kf_mean_x)):
    Bahn_Genauigkeit.append((((kf_mean_x[i] - leica_mean_x[i])**2) + ((kf_mean_y[i] - leica_mean_y[i])**2))**(1/2))

print(np.mean(Bahn_Genauigkeit))   
AT_bahn = np.max(Bahn_Genauigkeit)

Bahn_Genauigkeit = list(filter(lambda x : x != 0.0, Bahn_Genauigkeit))
print(np.min(Bahn_Genauigkeit))
print(np.mean(Bahn_Genauigkeit))   
AT_bahn = np.max(Bahn_Genauigkeit)
print(AT_bahn)


plt.rc('axes', axisbelow=True)

X = ['1','2','3','4', '5']
X_axis = np.arange(len(X))

plot1 = plt.figure(1)
plt.grid(True,axis = 'y')
plt.bar(X_axis - 0.2, rmse_x, 0.3, label = 'X',color='steelblue')
plt.bar(X_axis + 0.2, rmse_y, 0.3, label = 'Y',color='lightsteelblue')

plt.xticks(X_axis, X)
plt.xlabel("Zyklus")
plt.ylabel("RMSE [$mm$]")
plt.title("RMSE Achteck - v10",fontweight="bold")
plt.legend()

plt.show()
