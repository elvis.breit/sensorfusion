import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import uuid
import csv



rmse_x_v100 = [1.25,4.9,4.93,4.35,3.96]
rmse_x_v50 = [9.6,9.7,9.02,1.71,2.82]
rmse_x_v10 = [7.24,3.39,5.21,9.12,4.26]

rmse_y_v100 = [1.40,3.9,6.3,5.57,5.41]
rmse_y_v50 = [12.26,11.10,11.23,1.91,2.75]
rmse_y_v10 = [6.83,4.83,4.51,9.11,4.13]

bahn_genauigkeit = [15.25,18.03,10.3]
mean_bg = [6.75,8.87,5.09]
min_bg = [0.03,0.26,0.41]

plt.rc('axes', axisbelow=True)

X = ['1','2','3','4', '5']
X_axis = np.arange(len(X))

plot1 = plt.figure(1)
plt.grid(True,axis = 'y')
plt.bar(X_axis -0.2, rmse_x_v10, 0.15, label = 'v10',color='midnightblue')
plt.bar(X_axis, rmse_x_v50, 0.15, label = 'v50',color='steelblue')
plt.bar(X_axis + 0.2, rmse_x_v100, 0.15, label = 'v100',color='lightsteelblue')

plt.xticks(X_axis, X)
plt.xlabel("Zyklus")
plt.ylabel("RMSE [$mm$]")
plt.title("RMSE x-Richtung",fontweight="bold")
plt.legend()

plot2 = plt.figure(2)
plt.grid(True,axis = 'y')
plt.bar(X_axis -0.2, rmse_y_v10, 0.15, label = 'v10',color='midnightblue')
plt.bar(X_axis, rmse_y_v50, 0.15, label = 'v50',color='steelblue')
plt.bar(X_axis + 0.2, rmse_y_v100, 0.15, label = 'v100',color='lightsteelblue')

plt.xticks(X_axis, X)
plt.xlabel("Zyklus")
plt.ylabel("RMSE [$mm$]")
plt.title("RMSE y-Richtung",fontweight="bold")
plt.legend()

X = ['10','50','100']
X_axis = np.arange(len(X))

plot3 = plt.figure(3)
plt.grid(True,axis = 'y')
plt.bar(X_axis-0.2, bahn_genauigkeit,color='dimgray',width = 0.3,label='Maximum')
plt.bar(X_axis+0.2, mean_bg,color='darkgray',width = 0.3,label='Durchschnitt')

plt.xticks(X_axis, X)
plt.xlabel("Geschwindigkeit [$mm/s$]")
plt.ylabel("Bahn-Genauigkeit [$mm$]")
plt.title("Bahn-Genauigkeit",fontweight="bold")
plt.legend()

plt.show()
