#!/usr/bin/env python3
from typing import Type
from std_msgs.msg import Float64MultiArray, Float64
from sensorfusion.srv import Trajectory, TrajectoryResponse
import numpy as np
import rospy
import math
import csv

#Save X,Y Position in trajectory
with open('/home/pi/catkin_ws/src/sensorfusion/csv_path/octagon.csv', newline='') as csvfile:
        trajectory = np.loadtxt(csvfile, delimiter=',')


class traj(object):

    def __init__(self):

        #Perform first time in specific way
        self.not_init = True

        self.path_list = []

        self.segments_amount = len(trajectory)

        self.kf_calc_time = 0.11

        self.travel_time = 0.0

        s = rospy.Service('getPath_service',Trajectory, self.getPath_server)


    def calc_robot_direction_angle(self,current_pos:Type[Float64MultiArray], target_pos:Type[Float64MultiArray])->Float64:
        """Calculate direction of robot which it should use to reach target position from current position

        Args:
            current_pos (Type[Float64MultiArray]): current position where the robot is located (approximately)
            target_pos (Type[Float64MultiArray]): target position which should be reached

        Returns:
            Float64: return direction of robot
        """

        #X values identical, but difference in Y values. Determine driving forward or backward
        if current_pos[0] == target_pos[0]:
            if target_pos[1] > current_pos[1]:
                robot_direction = 90.0 #Robot drives forwards
                robot_angle = 90.0
            else:
                robot_direction = 270.0 #Robot drives backwards
                robot_angle = 270.0

        #Y values identical, but difference in X values. Determine driving left or right
        elif current_pos[1] == target_pos[1]:
            if target_pos[0] > current_pos[0]:
                robot_direction = 180.0 #Robot drives to the right
                robot_angle = 0.0
            else:
                robot_direction = 0 #Robot drives to the left
                robot_angle = 180.0

        #target point above
        elif target_pos[1] > current_pos[1]:

            #target point on left upper
            if current_pos[0] > target_pos[0]:

                #arctan(Perpendicular / Base) = angle
                robot_direction = np.arctan(abs(target_pos[1]-current_pos[1])/abs(target_pos[0]-current_pos[0]))*(180.0/np.pi) 
                robot_angle = np.arctan(abs(target_pos[1]-current_pos[1])/abs(target_pos[0]-current_pos[0]))*(180.0/np.pi) + 180.0
            
            #target point on right upper
            else:
                robot_direction = 90.0 + np.arctan((target_pos[0]-current_pos[0])/(target_pos[1]-current_pos[1]))*(180.0/np.pi)
                robot_angle = np.arctan((target_pos[1]-current_pos[1])/(target_pos[0]-current_pos[0]))*(180.0/np.pi)

        #target point below
        else:#target_pos[1] < current_pos[1]:

            #target point on left side
            if current_pos[0] > target_pos[0]:
                robot_direction = 270.0 + (np.arctan(abs(target_pos[0] - current_pos[0])/abs(target_pos[1] - current_pos[1])))*(180.0/np.pi)
                robot_angle = np.arctan((target_pos[1]-current_pos[1])/(target_pos[0]-current_pos[0]))*(180.0/np.pi) - 180.0

            #target point on right side
            else:
                robot_direction = 180.0 + np.arctan(abs(target_pos[1] - current_pos[1])/(target_pos[0] - current_pos[0]))*(180.0/np.pi)
                robot_angle = np.arctan(abs(target_pos[1] - current_pos[1])/(target_pos[0] - current_pos[0]))*(180.0/np.pi)

        return robot_direction, robot_angle

    def calc_travel_time(self,current_pos:Type[Float64MultiArray], target_pos:Type[Float64MultiArray], robot_velocity:Type[Float64])->Float64:
        """Calculate time which is needed to travel from current position to target position

        Args:
            current_pos (Type[Float64MultiArray]): current position where the robot is located (approximately)
            target_pos (Type[Float64MultiArray]): target position which should be reached

        Returns:
            Float64: calculated time
        """
        route_length = (abs(current_pos[0] - target_pos[0])**2 + abs(current_pos[1] - target_pos[1])**2)**(1/2)
        time = route_length / robot_velocity
        return time

    def getPath_server(self,request):
        """calculate direction, angle and time to travel for robot and forward service to client

        Args:
            request (_type_): request of client, inputs velocity and path_number

        Returns:
            TrajectoryResponse: Respond with angle, dt and direction 
        """

        #Save paths in path_list at specific intervals
        if(self.not_init):
            x_pos = []
            y_pos = []
            path_list = []

            #Save each X-Position in one list and each Y-Position in one list
            for i in range(len(trajectory)):
                x_pos.append(trajectory[i][0])
                y_pos.append(trajectory[i][1])
            
            #Calculate Lenghts of segments and save them in an array
            segment_lengths = (np.diff(x_pos)**2 + np.diff(y_pos)**2)**.5
            
            #Create cumsum_length variable to store the cumulative sum of lengths, see: https://www.geeksforgeeks.org/numpy-cumsum-in-python/
            cumsum_length = np.zeros_like(x_pos)
            
            #If only two Position points in csv are given, save the one value, so that array looks like [0, segment_lengths[0]]
            if(len(segment_lengths) == 1):
                np.delete(cumsum_length,0)
                cumsum_length[1] = segment_lengths[0]

            #If only three Position points in csv are given, save two values, so that array looks like [0, segment_lengths[0], segment_lengths[1]]
            elif(len(cumsum_length) == 2):
                cumsum_length[0] = segment_lengths[0]
                cumsum_length[1] = segment_lengths[0] + segment_lengths[1]

            #If more Position points given, use cumsum function and add every value
            else:    
                cumsum_length[1:] = np.cumsum(segment_lengths)
            
            #Interpolate to determine points of specific length intervals
            #From length 0 to the overall length, divide it into number of paths
            #Path interval is determined by trajectory length divided by velocity (v/s) 
            # which results in time needed to drive whole trajectory (t_traj)
            # divide t_traj by approximate calculation time of kalman filter given by prior measurements (here it is 0.1)
            #After optimatization, the value can be altered
            cumsum_length_int = np.linspace(0,cumsum_length.max(),math.floor((cumsum_length.max()/request.velocity)/self.kf_calc_time))
            x_pos_int = np.interp(cumsum_length_int, cumsum_length, x_pos)
            y_pos_int = np.interp(cumsum_length_int, cumsum_length, y_pos)
            
            #Save the values in a list and write them down in csv file
            for i in range(len(x_pos_int)):
                self.path_list.append([round(x_pos_int[i],2),round(y_pos_int[i],2)])

            with open('/home/pi/catkin_ws/src/sensorfusion/csv_path/path.csv', 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                writer.writerows(self.path_list)

            #End this save again by changing bool value to False
            self.not_init = False
        
        #No path information anymore? Return angle 999 to signal kalman filter that it has to stop and save data
        if(request.path_number >= len(self.path_list)):
            return TrajectoryResponse(angle=999, dt=self.travel_time, direction=0)

        #Work with indices to make it easier to understand
        i = request.path_number
        j = request.seg_number

        robot_direction, robot_angle = self.calc_robot_direction_angle(self.path_list[i-1],self.path_list[i])

        if (j < len(trajectory)-1):
            r_dir, seg_angle = self.calc_robot_direction_angle(trajectory[j],trajectory[j+1])
        else:
            seg_angle = 999        

        self.travel_time = self.calc_travel_time(self.path_list[i-1],self.path_list[i],request.velocity)
        segment_time = self.calc_travel_time(trajectory[j-1],trajectory[j],request.velocity)

        return TrajectoryResponse(angle=(round(robot_angle,2)), dt=(self.travel_time), direction=(robot_direction), seg_dt=(segment_time), seg_angle=(seg_angle))


if __name__=="__main__":
    rospy.init_node("trajectory")
    traj_obj = traj()
    rospy.spin()

