#!/usr/bin/env python3
import signal
import sys
import time
import numpy
import rospy
import serial
#from std_msgs.msg import Float64MultiArray
from sensorfusion.msg import Position
from typing import Type

values_prior = [0,0]
#values = Float64MultiArray()
values_float = [0,0]
fail = 0

sensor_adr = "/dev/ttyACM1"
baud_rate = 115200


def sensor_initialization() -> None:
    """Initialize sensor."""
    global sensor
    sensor = serial.Serial(sensor_adr, baud_rate, timeout=1)  # open serial port
    #time.sleep(1)
    rospy.sleep(1)
    print(sensor.readline())
    sensor.write(b"pxi\n")
    print(sensor.readline())
    sensor.write(b"ver\n")
    print(sensor.readline())

    sensor.write(b"xy2uart_on\n")  # activate sensor output

    # Read a couple of lines to clear output
    #time.sleep(1)
    rospy.sleep(1)
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print("Sensor initialization done!")


def sensor_readout() -> None:
    """Read sensor data and publish to topic 'pixart_V_values'"""
    #global values
    global values_prior
    global values_float
    global sensor
    
    #rate = rospy.Rate(120)  # 100hz
    while not rospy.is_shutdown():
        line = sensor.readline().strip().decode()
        values_string = numpy.array(line.split(";"))
        msg.x = round(float(values_string[0]) / 11.803, 4)
        msg.y = round(float(values_string[1]) / -11.966, 4)
        msg.x_var = 0.0 #will be calculated in kalman filter
        msg.y_var = 0.0 #will be calculated in kalman filter
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "/pixart_V_frame";       
        pub.publish(msg)
        #rate.sleep()
        


def exit() -> None:
   
    global sensor

    sensor.close()
    #print("Sensor closed")
    sys.exit(0)


if __name__ == "__main__":
    rospy.init_node("pixart_V_pub", anonymous=True)
    pub = rospy.Publisher("pixart_V_pos_cov", Position, queue_size=1)
    msg = Position()
    
    signal.signal(signal.SIGINT, exit)
    rospy.on_shutdown(exit)
    sensor_initialization()
    try:
        sensor_readout()
    except rospy.ROSInterruptException:
        pass
    
