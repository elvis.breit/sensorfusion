#!/usr/bin/env python
import signal
import sys
import time
from typing import Type

import numpy
import rospy
import serial
from pixart_ros.srv import PAA5102_service, PAA5102_serviceResponse

vector = [0, 0]
fail = [0, 0]


sensor_adr = "/dev/ttyACM0"     #adjust adress to match serial port
baud_rate = 115200


def sensor_initialization() -> None:
    """Initialize sensor."""
    global sensor
    sensor = serial.Serial(sensor_adr, baud_rate, timeout=1)  # open serial port
    time.sleep(1)
    print(sensor.readline())
    sensor.write(b"pxi\n")
    print(sensor.readline())
    sensor.write(b"ver\n")
    print(sensor.readline())

    sensor.write(b"xy2uart_on\n")  # activate sensor output

    # Read a couple of lines to clear output
    time.sleep(1)
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print("Sensor initialization done!")


def sensor_readout(req: Type[PAA5102_service]) -> Type[PAA5102_serviceResponse]:
    """Read sensor values.

    Args:
        req (Type[PAA5102]_service):  see service PAA5102_service.srv. Object with protery call and Response values.

    Returns:
        Type[PAA5102_serviceResponse]: Values at call time.
    """
    global vector
    global sensor
    global fail
    if req.call == 1:
        line = sensor.readline().strip().decode()
        vector_string = numpy.array(line.split(";"))
        try:
            vector[0] = float(vector_string[0])
            vector[1] = float(vector_string[1])
            return PAA5102_serviceResponse(vector)

        except:
            print("Line reading error!")
            return PAA5102_serviceResponse(fail)


def sensor_readout_server() -> None:
    """Start up server"""
    rospy.init_node("PAA5102_service")
    s = rospy.Service("sensor_readout", PAA5102_service, sensor_readout)
    rospy.spin()


def exit(signal, frame) -> None:
    """Exit function that makes sure to close serial port.

    Args:
        signal (any): dummy
        frame (any): dummy
    """
    sensor.close()
    print("Sensor closed")
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    sensor_initialization()
    sensor_readout_server()
