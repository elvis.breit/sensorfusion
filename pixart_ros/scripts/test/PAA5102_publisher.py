#!/usr/bin/env python
import signal
import sys
import time
import numpy
import rospy
import serial
from std_msgs.msg import Float64MultiArray
from typing import Type


values = Float64MultiArray()
values_float = [0,0]
fail = 0

sensor_adr = "/dev/ttyACM0"
baud_rate = 115200


def sensor_initialization() -> None:
    """Initialize sensor."""
    global sensor
    sensor = serial.Serial(sensor_adr, baud_rate, timeout=1)  # open serial port
    time.sleep(1)
    print(sensor.readline())
    sensor.write(b"pxi\n")
    print(sensor.readline())
    sensor.write(b"ver\n")
    print(sensor.readline())

    sensor.write(b"xy2uart_on\n")  # activate sensor output

    # Read a couple of lines to clear output
    time.sleep(1)
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print(sensor.readline())
    print("Sensor initialization done!")


def sensor_readout() -> None:
    """Read sensor data and publish to topic 'PAA5102_values'"""
    global values
    global values_float
    global sensor
    pub = rospy.Publisher("PAA5102_values", Float64MultiArray, queue_size=1)
    rospy.init_node("PAA5102_pub", anonymous=True)
    #rate = rospy.Rate(100)  # 100hz
    while not rospy.is_shutdown():
        line = sensor.readline().strip().decode()
        values_string = numpy.array(line.split(";"))
        try:
            values_float[0] = float(values_string[0])
            values_float[1] = float(values_string[1])
            values.data = values_float
            print(values.data)
            pub.publish(values)
            #rate.sleep()

        except:
            print("Line reading error!")
            values.data = [0,0]
            pub.publish(values)
            #rate.sleep()


def exit(signal, frame) -> None:
    """Exit function that makes sure to close serial port.

    Args:
        signal (any): dummy
        frame (any): dummy
    """
    sensor.close()
    print("Sensor closed")
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    sensor_initialization()
    try:
        sensor_readout()
    except rospy.ROSInterruptException:
        pass
