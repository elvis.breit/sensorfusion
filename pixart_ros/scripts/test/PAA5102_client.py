#!/usr/bin/env python
from typing import Type

import rospy
from pixart_ros.srv import PAA5102_service, PAA5102_serviceResponse


def start_server() -> None:
    """Start client and wait for service."""
    rospy.wait_for_service("sensor_readout")


def get_values(call: Type[PAA5102_service]) -> Type[PAA5102_serviceResponse]:
    """Call service and get values.

    Args:
        call (Type[PAA5102_service]): Service call

    Returns:
        Type[PAA5102_serviceResponse]: Sensor values
    """
    try:
        srv = rospy.ServiceProxy("sensor_readout", PAA5102_service)
        resp1 = srv(call)
        return resp1.values
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)


if __name__ == "__main__":
    call = 1
    start_server()
    while 1:
        print(get_values(call))
